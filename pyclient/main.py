from __future__ import unicode_literals
from __future__ import print_function
from AppKit import *
from Foundation import *


def printTabs(count):
    print('\t' * count, end='')


def printMenu(menu, level):
    printTabs(level)
    print("Title: {}\n".format(menu.title().UTF8String()), end='')
    printTabs(level)
    print("Items:\n", end='')
    for menuItem in menu.itemArray():
        printTabs(level+1)
        print("Title: {}\n".format(menuItem.title().UTF8String()), end='')
        if menuItem.submenu():
            printTabs(level+1)
            print("Submenu:\n", end='')
            printMenu(menuItem.submenu(), level+2)


def createMenu():
    menu = NSMenu.alloc().initWithTitle_("Client Menu")
    menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("C1", None, "")
    menu.addItem_(menuItem)

    menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("C2", None, "")
    menu.addItem_(menuItem)

    submenu = NSMenu.alloc().initWithTitle_("C2 Sub Menu")
    submenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("submenu item 1", None, ""))
    submenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("submenu item 2", None, ""))
    menu.setSubmenu_forItem_(submenu, menuItem)

    menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("C3", None, "")
    menu.addItem_(menuItem)
    return menu


def main ():
    connection = NSConnection.connectionWithRegisteredName_host_("server", None)
    backupServer = connection.rootProxy()

    menu = createMenu()
    print("Before calling server")
    printMenu(menu, 0)
    print("\nAfter calling server")
    backupServer.updateClientMenu_(menu)
    printMenu(menu, 0)
#    print("Calling server...")
#    menu = backupServer.backup()
#    printMenu(menu, 0)


if __name__ == '__main__':
    main()
