//
//  BOXAppDelegate.h
//  DOServer2
//
//  Created by Ryan Knotts on 11/1/13.
//  Copyright (c) 2013 Box. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BOXAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
