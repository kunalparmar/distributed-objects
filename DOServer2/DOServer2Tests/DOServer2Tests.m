//
//  DOServer2Tests.m
//  DOServer2Tests
//
//  Created by Ryan Knotts on 11/1/13.
//  Copyright (c) 2013 Box. All rights reserved.
//

#import "DOServer2Tests.h"

@implementation DOServer2Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in DOServer2Tests");
}

@end
