//
//  main.m
//  objcserver
//
//  Created by Kunal Parmar on 11/1/13.
//  Copyright (c) 2013 Kunal Parmar. All rights reserved.
//

@interface BackupServer : NSObject
@end

@implementation BackupServer

- (NSMenu *)createMenu
{
  NSMenu *menu = [[NSMenu alloc] initWithTitle:@"Server Menu"];

  NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:@"S1"
                                                    action:nil
                                             keyEquivalent:@""];
  [menu addItem:menuItem];

  menuItem = [[NSMenuItem alloc] initWithTitle:@"S2"
                                        action:nil
                                 keyEquivalent:@""];
  [menu addItem:menuItem];
  NSMenu *submenu = [[NSMenu alloc] initWithTitle:@"S2 Sub Menu"];
  [submenu addItem:[[NSMenuItem alloc] initWithTitle:@"submenu item 1"
                                              action:nil
                                       keyEquivalent:@""]];
  [submenu addItem:[[NSMenuItem alloc] initWithTitle:@"submenu item 2"
                                              action:nil
                                       keyEquivalent:@""]];
  [menu setSubmenu:submenu
           forItem:menuItem];

  menuItem = [[NSMenuItem alloc] initWithTitle:@"S3"
                                        action:nil
                                 keyEquivalent:@""];
  [menu addItem:menuItem];

  return menu;
}

- (NSMenu *)backup {
  printf("%s\n", __PRETTY_FUNCTION__);
  NSMenu *menu = [self createMenu];
  printf("Returning %s\n", [[menu description] UTF8String]);
  return menu;
}

- (void)updateClientMenu:(NSMenu *)menu
{
  printf("%s\n", __PRETTY_FUNCTION__);
  for (NSMenuItem *menuItem in [[self createMenu] itemArray])
  {
    [menu addItem:menuItem];
  }
  printf("Returning %s\n", [[menu description] UTF8String]);
}

@end

int main(int argc, const char * argv[])
{
  @autoreleasepool
  {
    NSConnection *connection = [NSConnection new];
    [connection setRootObject: [BackupServer new]];

    if ([connection registerName:@"server"] == NO)
    {
      printf("Impossible to vend this object\n");
    } else
    {
      printf("Object vended\n");
      [[NSRunLoop currentRunLoop] run];
    }
  }
  return 0;
}
