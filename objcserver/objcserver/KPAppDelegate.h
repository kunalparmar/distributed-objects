//
//  KPAppDelegate.h
//  objcserver
//
//  Created by Kunal Parmar on 11/1/13.
//  Copyright (c) 2013 Kunal Parmar. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KPAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
