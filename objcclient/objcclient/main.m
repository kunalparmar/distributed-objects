//
//  main.m
//  objcclient
//
//  Created by Kunal Parmar on 11/1/13.
//  Copyright (c) 2013 Kunal Parmar. All rights reserved.
//

@protocol BackupServerProtocol
- (NSMenu *)backup;
- (void)updateClientMenu:(NSMenu *)menu;
@end;

void printTabs(int count)
{
  while (count > 0)
  {
    printf("\t");
    count--;
  }
}

void printMenu(NSMenu *menu, int level)
{
  printTabs(level);
  printf("Title: %s\n", [[menu title] UTF8String]);
  printTabs(level);
  printf("Items:\n");
  for (NSMenuItem *menuItem in [menu itemArray])
  {
    printTabs(level+1);
    NSImage *image = [menuItem image];
    if (image)
    {
      NSBitmapImageRep *imageRep = [[image representations] objectAtIndex:0];
      NSData *data = [imageRep representationUsingType:NSPNGFileType
                                            properties: nil];
      [data writeToFile:@"/Users/kparmar/Desktop/menu_image.png"
             atomically:NO];
    }
    printf("Title: %s, image: %s\n", [[menuItem title] UTF8String], [image isValid]? "YES" : "NO");
    if ([menuItem submenu])
    {
      printTabs(level+1);
      printf("Submenu:\n");
      printMenu([menuItem submenu], level+2);
    }
  }
}

NSMenu *createMenu()
{
  NSMenu *menu = [[NSMenu alloc] initWithTitle:@"Client Menu"];

  NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:@"C1"
                                                    action:nil
                                             keyEquivalent:@""];
  [menu addItem:menuItem];

  menuItem = [[NSMenuItem alloc] initWithTitle:@"C2"
                                        action:nil
                                 keyEquivalent:@""];
  [menu addItem:menuItem];
  NSMenu *submenu = [[NSMenu alloc] initWithTitle:@"C2 Sub Menu"];
  [submenu addItem:[[NSMenuItem alloc] initWithTitle:@"submenu item 1"
                                              action:nil
                                       keyEquivalent:@""]];
  [submenu addItem:[[NSMenuItem alloc] initWithTitle:@"submenu item 2"
                                              action:nil
                                       keyEquivalent:@""]];
  [menu setSubmenu:submenu
           forItem:menuItem];

  menuItem = [[NSMenuItem alloc] initWithTitle:@"C3"
                                        action:nil
                                 keyEquivalent:@""];
  [menu addItem:menuItem];

  return menu;
}

int main (int argc, const char * argv[]) {
  @autoreleasepool {
    NSConnection *connection= [NSConnection connectionWithRegisteredName:@"server"
                                                                    host:nil];
    id backupServer = [connection rootProxy];
    [backupServer setProtocolForProxy:@protocol(BackupServerProtocol)];

    NSMenu *menu = createMenu();
    printf("Before calling server\n");
    printMenu(menu, 0);
    [backupServer updateClientMenu:menu];
    printf("\nAfter calling server\n");
    printMenu(menu, 0);

//    NSMenu *menu = [backupServer backup];
//    printf("Menu from server\n");
//    printMenu(menu, 0);
  }
  return 0;
}
