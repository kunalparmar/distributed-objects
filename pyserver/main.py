from __future__ import unicode_literals
from __future__ import print_function
from AppKit import *
from Foundation import *
from objc import *

class BackupServer(NSObject):

    def createMenu(self):
        image = NSImage.alloc().initByReferencingFile_("app.png")
        if not image:
            print("image not found\n")
        menu = NSMenu.alloc().initWithTitle_("Server Menu")
        menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("S1", None, "")
        menuItem.setImage_(image)
        menu.addItem_(menuItem)

        menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("S2", None, "")
        menu.addItem_(menuItem)

        submenu = NSMenu.alloc().initWithTitle_("S2 Sub Menu")
        submenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("submenu item 1", None, ""))
        submenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("submenu item 2", None, ""))
        menu.setSubmenu_forItem_(submenu, menuItem)

        menuItem = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("S3", None, "")
        menu.addItem_(menuItem)
        return menu

    @typedSelector(b'@@:')
    def backup(self):
        try:
            print("backup\n")
            return self.createMenu()
        except Exception as ex:
            print("Exception {}".format(ex))
            raise

    @typedSelector(b'v@:@')
    def updateClientMenu_(self, menu):
        print("updateClientMenu\n")
        for menuItem in self.createMenu().itemArray():
            menu.addItem_(menuItem)
        print("Returning {}\n", menu);


def main():
    connection = NSConnection.new()
    connection.setRootObject_(BackupServer.new())

    if not connection.registerName_("server"):
      print("Impossible to vend this object.\n")
    else:
      print("Object vended.\n")
      NSRunLoop.currentRunLoop().run()


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        print("Exception {}".format(ex))
